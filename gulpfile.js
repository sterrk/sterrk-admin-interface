var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minifier = require('gulp-uglify/minifier');
var uglifyjs = require('uglify-js-harmony');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('gulp-run-sequence');

var packageFilename = 'sterrk-admin-interface';
var assetsPath = 'src/assets/';
var distPath = 'dist/';

// DEVELOPMENT
gulp.task('build', function() {
    runSequence(['build-css', 'build-js', 'copy-assets']);
});

// PRODUCTION
gulp.task('production', function() {
    runSequence(['build-css', 'build-js', 'copy-assets'], ['minify-css', 'minify-js']);
});

// Default task
gulp.task('default',function() {
    gulp.start('watch');
});

// Watch task
gulp.task('watch',function() {
    gulp.start('build');
    gulp.watch('src/scss/**/*.scss',['build']);
    gulp.watch('src/js/**/*.js',['build']);
});

// Build tasks
gulp.task('build-css', function(){
    return gulp.src('src/scss/main.scss')
        .pipe(sass())
        .pipe(rename(packageFilename+'.css'))
        .pipe(gulp.dest(distPath));
});

gulp.task('build-js', function(){
    return gulp.src([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/jquery-form/src/jquery.form.js',
            'node_modules/foundation-sites/dist/js/foundation.js',
            'node_modules/foundation-datepicker/js/foundation-datepicker.js',
            'node_modules/datatables.net/js/jquery.dataTables.js',
            'node_modules/datatables.net-zf/js/dataTables.foundation.js',
            'src/js/utilities/*.js',
            'src/js/components/*.js',
            'src/js/templates/*.js',
            'src/js/init.js'
        ])
        .pipe(concat(packageFilename+'.js'))
        .pipe(gulp.dest(distPath));
});

gulp.task('copy-assets', function(){
    return gulp.src(assetsPath+'**/*')
        .pipe(gulp.dest('sai-assets'));
});

// Production tasks
gulp.task('minify-css', function(){
    return gulp.src(distPath+packageFilename+'.css')
        .pipe(rename(packageFilename+'.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(distPath));
});

gulp.task('minify-js', function(){
    return gulp.src(distPath+packageFilename+'.js')
        .pipe(concat(packageFilename+'.min.js'))
        .pipe(minifier({}, uglifyjs).on('error', function(e){
            console.log(e);
         }))
        .pipe(gulp.dest(distPath));
});