var auth0Client = new Auth0({
    domain:       '<YOUR_DOMAIN>',
    clientID:     '<YOUR_CLIENT_ID>',
    callbackURL:  '<YOUR_CALLBACK_URL>',
    responseType: 'token'
});

$(function(){
    $('#submitLogin').on('click', function(){
      doLogin();
    });
});

function doLogin() {
  var options = $.extend({}, [], {
    connection: 'Username-Password-Authentication',
    username: $('#email').val(),
    password: $('#password').val()
  });
  auth0Client.login(options);
}
