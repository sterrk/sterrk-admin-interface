function SAIUtilities() {

    this.createScrollbox = function(element) {
        element.addClass('sai-scrollbox-holder');
        element.html(
            "<div class='sai-scrollbox-content'>" +
            element.html() +
            "</div>"
        );
        return element;
    }

    this.createExternalScrollbox = function(element) {
        element.addClass('sai-scrollbox-content');
        return "<div class='sai-scrollbox-holder'>" +
                element[0].outerHTML +
            "</div>";
    }
}
