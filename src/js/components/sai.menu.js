$(function(){
    new SAIMenuController();
});

function SAIMenuHorizontal(element) {

    this.element = element;

    this._init = function() {
        this.hideSubmenus();
        this._initHovers();
        this._initActiveParents();
    }

    this._initHovers = function() {
        this.element.find('.menu-items li:not(.group)').hover(this._openSubmenu, this._closeSubmenu);
        this.element.find('.submenu').hover(this._openSubmenu, this._closeSubmenu);
    }

    this._initActiveParents = function() {
        let menu = this.element;
        this.element.find('.submenu li.active').each(function(){
            let submenuId = $(this).closest('.submenu').attr('data-submenu');
            menu.find('li > a[data-submenu-trigger="'+submenuId+'"]').parent().addClass('active');
        });
    }

    this._openSubmenu = function(event) {
        let menu = $(event.target.closest('.sai-menu'));
        let anchor = $(this).find('a');
        let submenutrigger = anchor.attr('data-submenu-trigger');
        let submenu = menu.find('.submenu[data-submenu="'+submenutrigger+'"]');
        if ( submenu.length === 1 ) {
            submenu.slideDown(100);
        }
    }

    this._closeSubmenu = function(event) {
        if( // Mouseout moet niet eindigen in een submenu
            event.relatedTarget === null
            || event.relatedTarget.closest('.submenu') === null
            || event.relatedTarget.closest('.submenu').lenght < 1
        ) {
            let menu = $(event.currentTarget.closest('.sai-menu'));
            menu.find('.submenu:visible').each(function(){
                $(this).slideUp(100);
            });
        }
    }

    this.hideSubmenus = function() {
        this.element.find('.submenu').hide();
    }

    this._init();
}

function SAIMenuVertical(element) {

    this.element = element;
    this.menuItems = [];
    this.submenus = [];

    this._init = function() {

        SAIUtil = new SAIUtilities();
        this.element = SAIUtil.createScrollbox(this.element);

        let menuObject = this;
        this.element.find('.menu-items li').each(function(){
            let menuItem = new SAIMenuItemVertical($(this), menuObject);
            menuObject.menuItems.push(menuItem);
        });
    }

    this.getMenuItemByElement = function(elem) {
        for (index in this.menuItems) {
            let menuItemObject = this.menuItems[index];
            if (menuItemObject.element.get(0) == elem.get(0)) {
                return menuItemObject;
            }
        }
        return null;
    }

    this._init();
}

function SAIMenuItemVertical(element, menu) {

    this.element = element;
    this.menu = menu;
    this.submenus = [];

    this._init = function() {
        this._initSubmenus();
        this._initIcon();
        this._initActive();
        this._initActiveParents();
        this.element.find('> a').click(this, this._handleClick);
    }

    this._handleClick = function(event) {
        let menuItemObject = event.data;
        menuItemObject.toggleActive();
    }

    this._initSubmenus = function() {
        this.submenus = this.element.find('> ul');
    }

    this._initIcon = function() {
        if ( this.hasSubmenus() ) {
            this.element.addClass('show-icon');
        }
    }

    this._initActive = function() {
        this.isActive() ? this.setActive() : this.setInactive();
    }

    this._initActiveParents = function() {
        if ( this.isActive() ) {
            let parents = this.getParentMenuItems();
        }
    }

    this.toggleActive = function() {
        this.isActive() ? this.setInactive() : this.setActive();
    }

    this.setActive = function() {
        this.element.addClass('active');
        this.showSubmenus();
    }

    this.setInactive = function() {
        this.element.removeClass('active');
        this.hideSubmenus();
    }

    this.isActive = function() {
        return this.element.hasClass('active');
    }

    this.hasSubmenus = function() {
        return this.submenus.length > 0;
    }

    this.getSubmenus = function() {
        return this.submenus;
    }

    this.showSubmenus = function() {
        if ( this.hasSubmenus() ) {
            this.submenus.slideDown(100);
        }
    }

    this.hideSubmenus = function() {
        if ( this.hasSubmenus() ) {
            this.submenus.slideUp(100);
        }
    }

    this.getParentMenuItems = function() {
        let menu = this.menu;
        let parents = this.element.parents('ul > li');
        if ( parents.length > 0 ) {
            parents.each(function(){
                let menuItemObject = menu.getMenuItemByElement($(this));
                menuItemObject.setActive();
            });
        }
    }

    this._init();
}

function SAIMenuController(selector) {

    this.selector = selector ? selector : '.sai-menu';
    this._init = function() {
        $(this.selector).each(function(){
            if ( $(this).hasClass('vertical') ) {
                new SAIMenuVertical($(this));
            } else {
                new SAIMenuHorizontal($(this));
            }
        });
    }

    this._init();
}
