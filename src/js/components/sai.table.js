$(function(){
    new SAITableController();
});

function SAITable(element) {

    this.element = element;
    this._init = function() {
        if ( this.element.hasClass('dataTable') ) {
            this.initDataTable();
        }
    }

    this.initDataTable = function() {
        this.element.on( 'init.dt', {tableObject: this}, this._doSAIMutations);
        this.element.DataTable();
    }

    this._doSAIMutations = function(event, settings, json) {
        let tableObject = event.data.tableObject;
        tableObject.addSAIClasses();
        tableObject.setSAICSS();
    }

    this.addSAIClasses = function() {
        this._getInputElements().addClass('sai');
    }

    this.setSAICSS = function() {
        this._getInputElements().css('width', 'auto');
    }

    this._getInputElements = function() {
        return this.element.closest('.dataTables_wrapper').find('select, input');
    }

    this._init();
}

function SAITableController(selector) {

    this.selector = selector ? selector : '.sai-table';
    this._init = function() {
        $(this.selector).each(function(){
            new SAITable($(this));
        });
    }

    this._init();
}
