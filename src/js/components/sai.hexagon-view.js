$(function(){
    new SAIHexagonViewController();
});

function SAIHexagonView(element) {

    this.element = element;
    this._init = function() {

        this.element.find('.hexagons').each(function(){
            let childcount = $(this).children().length;
            $(this).attr('data-itemcount', childcount);
        });
    }

    this._init();
}

function SAIHexagonViewController(selector) {

    this.selector = selector ? selector : '.sai-hexagon-view';
    this._init = function() {
        $(this.selector).each(function(){
            new SAIHexagonView($(this));
        });
    }

    this._init();
}
