$(function(){
    new SAIPanelController();
});

function SAIPanel(element) {

    var status_open = 'open';
    var status_closed = 'closed';
    var class_open = 'open';

    this.element = element;
    this.status = status_open;
    this.header = null;
    this.content = null;
    this.footer = null;
    this.trigger = null;
    
    this._init = function() {
        this.header = this.element.find('>.header');
        this.content = this.element.find('>.content');
        this.footer = this.element.find('>.footer');
        this.trigger = this.element.find('>.header .trigger');
        
        this.status = this.element.hasClass('collapsed') ? status_closed : status_open;

        this.header.click(this, this._triggerStatus);
        this.header.find('ul.buttons > li').click(this, this._preventTriggerStatus);
        
        this.setTriggerIcon();

        if ( !this.isOpen() ) {
            this.content.hide();
            this.footer.hide();
        }
    }

    this._triggerStatus = function(event) {
        event.data.isOpen() ? event.data.close() : event.data.open();
    }
    
    this._preventTriggerStatus = function(event) {
        if ( event.target !== event.data.trigger.get(0) ) { // Only stop propagation if it is an element other than the trigger
            event.stopPropagation();
        }
    }

    this.setStatus = function(status) {
        this.status = status;
    }

    this.isOpen = function() {
        return this.status === status_open;
    }

    this.open = function() {
        this.setStatus(status_open);
        this.setTriggerIcon();
        
        this.element.removeClass('collapsed');
        this.content.slideDown(200);
        this.footer.slideDown(200);
    }

    this.close = function() {
        this.setStatus(status_closed);
        this.setTriggerIcon();
        
        this.element.addClass('collapsed');
        this.content.slideUp(200);
        this.footer.slideUp(200);
    }

    this.setTriggerIcon = function() {
        if ( this.isOpen() ) {
           this.trigger.addClass(class_open);
        } else {
           this.trigger.removeClass(class_open);
        }
    }

    this._init();
}

function SAIPanelController(selector) {

    this.selector = selector ? selector : '.sai-panel';
    this._init = function() {
        $(this.selector).each(function(){
            new SAIPanel($(this));
        });
    }

    this._init();
}
