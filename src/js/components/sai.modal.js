$(function(){
    new SAIModalController();
});

function SAIModal(element) {

    this.element = element;
    this._init = function() {
        this.element.addClass('reveal');
        new Foundation.Reveal(this.element, {});
    }

    this._init();
}

function SAIModalController(selector) {

    this.selector = selector ? selector : '.sai-modal';
    this._init = function() {
        $(this.selector).each(function(){
            new SAIModal($(this));
        });
    }

    this._init();
}
