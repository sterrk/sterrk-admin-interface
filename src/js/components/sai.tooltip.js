$(function(){
    new SAITooltipController();
});

function SAITooltip(element) {

    this.element = element;
    this._init = function() {
        this.element.addClass('has-tooltip');
        this.element.addClass('top');
        new Foundation.Tooltip(this.element, {
            allowHtml: true,
            clickOpen: true
        });
    }

    this._init();
}

function SAITooltipController(selector) {

    this.selector = selector ? selector : '.sai-tooltip';
    this._init = function() {
        $(this.selector).each(function(){
            new SAITooltip($(this));
        });
    }

    this._init();
}
