$(function(){
    new SAISidebarController();
});

function SAISidebar(element) {

    var position_left = 'left';
    var position_right = 'right';
    var status_open = 'open';
    var status_closed = 'closed';
    var class_open = 'open';

    this.element = element;
    this.status = status_open;
    this.position = position_right;

    this._init = function() {

        SAIUtil = new SAIUtilities();
        this.element.find('.content').replaceWith(SAIUtil.createScrollbox(this.element.find('.content')));

        this.element.find('.trigger').click(this, this._triggerStatus);
        this.status = this.element.hasClass('collapsed') ? status_closed : status_open;
        this.position = this.element.hasClass('left') ? position_left : position_right;

        this.setTriggerIcon();
    }

    this._triggerStatus = function(event) {
        event.data.isOpen() ? event.data.close() : event.data.open();
    }

    this.setStatus = function(status) {
        this.status = status;
    }

    this.isOpen = function() {
        return this.status === status_open;
    }

    this.open = function() {
        this.setStatus(status_open);
        this.setTriggerIcon();
        this.element.removeClass('collapsed');
    }

    this.close = function() {
        this.setStatus(status_closed);
        this.setTriggerIcon();
        this.element.addClass('collapsed');
    }

    this.setTriggerIcon = function() {
        if ( this.isOpen() ) {
           this.element.find('.trigger').addClass(class_open);
        } else {
           this.element.find('.trigger').removeClass(class_open);
        }
    }

    this._init();
}

function SAISidebarController(selector) {

    this.selector = selector ? selector : '.sai-sidebar';
    this._init = function() {
        $(this.selector).each(function(){
            new SAISidebar($(this));
        });
    }

    this._init();
}
